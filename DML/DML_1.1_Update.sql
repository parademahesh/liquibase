--liquibase formatted sql

--changeset author:Vijay_D labels:DEMO context:DEV
--comment: Update in Email address
UPDATE person
SET Email = 'Peter.Mike@outlook.com'
WHERE ID = 5;

--changeset author:suresh labels:DEMO context:DEV
--comment: Select Person table
SELECT * FROM person;
