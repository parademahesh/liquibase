--liquibase formatted sql

--changeset developer.vijay:1 labels:Demo context:DEV
--comment: example comment
create table person (
    id int primary key auto_increment not null,
    name varchar(50) not null,
    address1 varchar(50),
    address2 varchar(50),
    city varchar(30)
)
--rollback DROP TABLE person;

--changeset developer.vimal:2 labels:Demo context:DEV
--comment: example comment
create table company (
    id int primary key auto_increment not null,
    name varchar(50) not null,
    address1 varchar(50),
    address2 varchar(50),
    city varchar(30)
)
--rollback DROP TABLE company;

--changeset developer.vijay:3 labels:Demo context:DEV
--comment: example comment
alter table person add column country varchar(2)
--rollback ALTER TABLE person DROP COLUMN country;

--changeset developer.vijay:4 labels:Demo context:DEV
--comment: example comment
alter table person add column Email varchar(2)
--rollback ALTER TABLE person DROP COLUMN country;
